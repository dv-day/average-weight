import React from "react";
import "./App.css";
import InputComp from "./components/InputComp.js";
import SummComp from "./components/SummComp.js";
import DispComp from "./components/DispComp.js";
import { Button } from "react-bootstrap";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      persons: [],
      avg: 0
    };
  }

  getPerson = person => {
    const newArr = [...this.state.persons, person];
    this.setState({
      persons: newArr
    });
    console.log(this.state.persons);
    this.calSum(newArr);
  };

  clearData = () => {
    this.setState({ persons: [], avg: 0 });
  };

  calSum = newArr => {
    let summary = 0;
    for (let i = 0; i < newArr.length; i++) {
      summary += parseInt(newArr[i].weight);
    }
    // summary / newArr.length
    this.setState({
      avg: summary / newArr.length
    });
    // console.log(this.state.sum);
  };
  
  render() {
    return (
      <div className="App">
        {/*        v Use in props  v Use in this */}
        <InputComp onSubmit={this.getPerson} />
        <DispComp tableData={this.state.persons} />
        <SummComp avg={this.state.avg} />
        <Button variant="outline-danger" size="lg" onClick={this.clearData}>
          Clear
        </Button>
      </div>
    );
  }
}

export default App;

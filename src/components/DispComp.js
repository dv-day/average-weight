import React from "react";
import { Table } from "react-bootstrap";


class DispComp extends React.Component {
  //use props.tableData
 
  addTable = () => {
    // console.log(props.tableData);
    return this.props.tableData.map((item, i) => (
      <tr>
        <td key={i}>{i + 1}</td>
        <td>{item.name}</td>
        <td>{item.weight}</td>
      </tr>
    ));
  };
  
  render() {
    return (
      <div>
        <Table style={{ width: "80%",margin:"20px 10%" }} striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Weight</th>
            </tr>
          </thead>
          <tbody>{this.addTable()}</tbody>
        </Table>
      </div>
    );
  }
}

export default DispComp;

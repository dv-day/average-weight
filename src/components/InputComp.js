import React from "react";
import { Button } from "react-bootstrap";

class InputComp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      weight: ""
    };
  }

  onSubmit = () => {
    if (this.state.name == "" || this.state.weight == "") {
      alert("error");
    } else {
      this.props.onSubmit(this.state);
      this.setState({
        name: "",
        weight: ""
      });
    }

    console.log(this.state.name + " " + this.state.weight);
  };

  handleChangeName = event => {
    this.setState({
      name: event.target.value
    });
  };

  handleChangeWeight = event => {
    this.setState({
      weight: event.target.value
    });
  };

  render() {
    return (
      <div>
        <input
          type="text"
          placeholder="Name"
          value={this.state.name}
          onChange={this.handleChangeName}
        ></input>

        <input
          type="number"
          placeholder="Weight"
          value={this.state.weight || ""}
          onChange={this.handleChangeWeight}
        ></input>

        <Button variant="outline-success" type="submit" onClick={this.onSubmit}>
          Add
        </Button>
      </div>
    );
  }
}

export default InputComp;

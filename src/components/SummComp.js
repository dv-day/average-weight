import React from "react";

class SummComp extends React.Component {
  
  render() {
    return (
      <div>
          <h1>Average weight is {this.props.avg}</h1>
        
      </div>
    );
  }
}

export default SummComp;
